import face_recognition
from knn_traning import train
import pickle
from datetime import datetime
import cv2
import os
import smtplib
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from knn_traning import train
import cv2
import numpy as np
import os

cv2.namedWindow('Frame',cv2.WINDOW_NORMAL)

def convert_frames_to_video(pathIn,pathOut,fps):
	frame_array = []
	files = [f for f in os.listdir(pathIn) if isfile(join(pathIn, f))]
 
	#for sorting the file names properly
	files.sort(key = lambda x: int(x[5:-4]))
 
	for i in range(len(files)):
		filename=pathIn + files[i]
		#reading each files
		img = cv2.imread(filename)
		height, width, layers = img.shape
		size = (width,height)
		print(filename)
		#inserting the frames into an image array
		frame_array.append(img)
 
	out = cv2.VideoWriter(pathOut,cv2.VideoWriter_fourcc(*'DIVX'), fps, size)
 
	for i in range(len(frame_array)):
		# writing to a image array
		out.write(frame_array[i])
	out.release()

def SendMail(ImgFileName):
	img_data = open(ImgFileName, 'rb').read()
	msg = MIMEMultipart()
	msg['Subject'] = 'subject'
	msg['From'] = 'loksunt@gmail.com'
	msg['To'] = 'take2rohit@gmail.com'

	text = MIMEText("Intruder")
	msg.attach(text) 
	image = MIMEImage(img_data, name=os.path.basename(ImgFileName))
	msg.attach(image)

	s = smtplib.SMTP('smtp.gmail.com', 587)
	s.ehlo()
	s.starttls()
	s.ehlo()
	s.login('loksunt@gmail.com', 'Loksun@2018')
	s.sendmail('loksunt@gmail.com', 'take2rohit@gmail.com', msg.as_string())
	# s.quit()

def predict(X_img, knn_clf=None, model_path=None, distance_threshold=0.4):
	if knn_clf is None:
		with open(model_path, 'rb') as f:
			knn_clf = pickle.load(f)

	X_face_locations = face_recognition.face_locations(X_img,model='hog')

	if len(X_face_locations) == 0:
		return []

	faces_encodings = face_recognition.face_encodings(X_img, known_face_locations=X_face_locations)

	closest_distances = knn_clf.kneighbors(faces_encodings, n_neighbors=1)

	are_matches = [closest_distances[0][i][0] <= distance_threshold for i in range(len(X_face_locations))]

	return [(pred, loc) if rec else ("unknown", loc) for pred, loc, rec in zip(knn_clf.predict(faces_encodings), X_face_locations, are_matches)]



# train('training_faces','trained')
flag = 1
prev_sec = 0
authorised = []
unknown_list = []
cam = cv2.VideoCapture(0)
uk_count = 0 
count = 0
new = 0
fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter('output.avi',fourcc, 20.0, (640,480))
scale = 0.25

while True:
	# if datetime.now().time().minute > 45:
	# 	print('terminated')
	# 	break
	count+=1
	ret,frame = cam.read()
	current_second = datetime.now().time().second
	current_minute = datetime.now().time().minute
	if frame is not None:
		rgb = cv2.resize(frame, (0, 0), fx=scale, fy=scale)
		font = cv2.FONT_HERSHEY_DUPLEX
		cv2.putText(frame,datetime.now().strftime("%H:%M:%S (%d-%m-%Y)"), (30, 30), font, 1, (255, 255, 255), 2)
		predictions = predict(rgb, model_path='trained')
		for (name,(top, right, bottom, left)) in predictions:
			top *= int(1/scale)
			right *= int(1/scale)
			bottom *= int(1/scale)
			left *= int(1/scale)
			# Draw a box around the face
			cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)
			# Draw a label with a name below the face
			cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
			cv2.putText(frame, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)
			

			if name == "unknown" and count > 10:
				out.write(frame)
				if current_second % 3 == 0 and flag == 1:
					new+=1  

					cv2.imwrite('unknowns/{}.png'.format(new),frame)
					print('{}. Image Saved'.format(new), current_second)

					SendMail('unknowns/{}.png'.format(new))
					print('{}. Mail Sent... \n'.format(new))
					
					flag = 0
					prev_sec = current_second

				if current_second == prev_sec+15:
					flag = 1
			
			elif name !='unknown' and name not in authorised:
				authorised.append(name)
				print('{} detected'.format(name))
				cv2.imwrite('detected_face/{}.png'.format(name),frame)
		cv2.imshow("Frame", frame)
		if cv2.waitKey(1) & 0xFF == ord('q'):
			break

cam.release()
cv2.destroyAllWindows()

# SendMail('output.avi')

# cap = cv2.VideoCapture('output.avi')
# length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
# print(length)
# if length<1:
# 	os.remove('output.avi')